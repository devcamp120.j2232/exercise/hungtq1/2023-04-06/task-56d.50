package com.devcamp.restapibookauthorv2.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.restapibookauthorv2.models.Author;

@Service
public class AuthorService {
  Author author1 = new Author("Nguyen Van A", "a@gamil.com", 'm');
  Author author2 = new Author("Pham Thi B", "b@gamil.com", 'f');
  Author author3 = new Author("Le Chi C", "c@gamil.com", 'm');
  Author author4 = new Author("Nguyen Van D", "d@gamil.com", 'm');
  Author author5 = new Author("Pham The E", "e@gamil.com", 'f');
  Author author6 = new Author("Le Vinh F", "f@gamil.com", 'm');

  ArrayList<Author> authors = new ArrayList<>();

  public ArrayList<Author> getAuthors() {
    return authors;
  }

  public ArrayList<Author> getAuthor1() {
    ArrayList<Author> listAuthor1 = new ArrayList<>();
    listAuthor1.add(author1);
    listAuthor1.add(author2);

    return listAuthor1;
  }

  public ArrayList<Author> getAuthor2() {
    ArrayList<Author> listAuthor2 = new ArrayList<>();
    listAuthor2.add(author3);
    listAuthor2.add(author4);

    return listAuthor2;
  }

  public ArrayList<Author> getAuthor3() {
    ArrayList<Author> listAuthor3 = new ArrayList<>();
    listAuthor3.add(author5);
    listAuthor3.add(author6);

    return listAuthor3;
  }

  public ArrayList<Author> getAllAuthor() {
    ArrayList<Author> allAuthor = new ArrayList<>();
    allAuthor.addAll(getAuthor1());
    allAuthor.addAll(getAuthor2());
    allAuthor.addAll(getAuthor3());

    return allAuthor;
  }

  public Author getAuthorByEmail(String email) {
    ArrayList<Author> allAuthor = new ArrayList<>();
    allAuthor.addAll(getAllAuthor());
    Author author = new Author();
    for (int i = 0; i < allAuthor.size(); i++) {
      if (allAuthor.get(i).getEmail().equals(email))
        author = allAuthor.get(i);
    }

    return author;
  }

  public ArrayList<Author> getAuthorByGender(char gender) {
    ArrayList<Author> allAuthor = new ArrayList<>();
    allAuthor.addAll(getAllAuthor());
    ArrayList<Author> author = new ArrayList<>();
    for (int i = 0; i < allAuthor.size(); i++) {
      if (allAuthor.get(i).getGender() == gender)
        author.add(allAuthor.get(i));
    }

    return author;
  }
}
